﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joro
{
    public class Calculator
    {
        public static int Add(int a, int b) => a + b;
        public static int Subtract(int a, int b) => a - b;
        public static int Multiply(int a, int b) => a * b;
        public static double Divide(int a, int b) => a / b;
    }
}